#include "System_Config.h"
#include "math.h"
#include <acc_comp.h>
#include <Matrix.h>

float q_error[4]={0,0,0,0};//四元数、陀螺零位EKF滤波使用参数
float g=9.82;
float pi=3.1416;
float DX,DY,DZ;
float* ACCk1[3]={0}; 
float* cbnkk[9]={0};
float* cbnkkf[9]={0};

float* acc_off[3]={0};
float* ACCk2[3]={0};
float wf[3]={0};
float wf1[3]={0};
float wf2[3]={0};
float cbn[9]={0};
float q_b[4]={0};
float q_errortemp0=0;
float q_errortemp1=0;
float q_errortemp2=0;
float* W[3]={0};
float* init_angle[3];
float* angle[3];
float model_q;
float TT[3][3];
float init_T11,init_T12,init_T13,init_T21,init_T22,init_T23,init_T31,init_T32,init_T33;
float d[4];
float dd[4];
float hx,hy,hz;
float* m[3];
float* MT[3];
float* f[3];
float* MA[3];

float insdv[3]={0};
float aggg[3]={0};
float mpusumx=0;
float mpusumy=0;
float mpusumz=0;
float accsumx=0;
float accsumy=0;
float accsumz=0;
float insv[3]={0};
float inss[3]={0};
float insg[3]={0}; 
float insdv_init[3]={0};
float inss_init[3]={0};
float wx_a=0;
float wy_a=0;
float wz_a=0;

extern int flag;
extern float seq[4];
extern float mpuoffsetx;
extern float mpuoffsety;
extern float mpuoffsetz;
extern float mpugsetx;
extern float mpugsety;
extern float mpugsetz;
extern float mpugkx;
extern float mpugky;
extern float mpugkz;
extern  float mpukx;
extern  float mpuky;
extern  float mpukz;
extern float kk0,kk1,kk2,kk3;
float PP[16]={0};
int   gyrodatasss[3]={0};
int   accggg[3]={0};
int gryoo[3]={0};
int flagzuhe=1;
/*下面是误差四元数使用的变量*/
float kf_t=1;
float xk[9]={0};
float FQ[81]={0};
float RA[3]={0};
float RM[3]={0};
float ZAK[3]={0};
float ZMK[3]={0};
float xkerror[9]={0};
float PM[81]={0};
float PPA[81]={0};
float GE=0;
float GN=0;
float GU=1;
int yuce_only=0;
/*电子罗盘程序*/
void compass(float* f,float* MA,float* m,float* MT)
{
  float mpitch,hx,hy=0;
  
  MT[0]=f[0];
  MT[1]=f[1];
  MT[2]=f[2];
  
  mpitch=-asin(MT[0]);
  m[1]=mpitch;
  m[2]=atan2(MT[1],MT[2]);
  
  ///根据惯性技术书上，应该是这个样子,网上得坐标轴定义和一般得惯性定义不一致
  /*Mn=cnb*Mb */
  hx=MA[0]*cos(m[1])+MA[1]*sin(m[2])*sin(m[1])+MA[2]*cos(m[2])*sin(m[1]);
  hy=MA[1]*cos(m[2])-MA[2]*sin(m[2]);
  hz=-MA[0]*sin(m[1])+MA[1]*sin(m[2])*cos(m[1])+MA[2]*cos(m[2])*cos(m[1]);
  /*判断航向角度的象限*/
  if(hx==0&&hy<0)
  {
    m[0]=pi;
  }
  if(hx==0&&hy>0)
  {
    m[0]=0;
  }
  if(hy==0&&hx<0)
  {
    m[0]=1.5*pi;
  }
  if(hy==0&&hx>0)
  {
    m[0]=0.5*pi;
  }
  if(hx<0&&hy>0)
  {
    m[0]=1.5*pi-atan(hy/hx);
  }
  if(hx<0&&hy<0)
  {
    m[0]=1.5*pi-atan(hy/hx);
  }
  if(hx>0&&hy<0)
  {
    m[0]=0.5*pi-atan(hy/hx);
  }
  if(hx>0&&hy>0)
  {
    m[0]=0.5*pi-atan(hy/hx);
  }
}
/*欧拉角度转四元数代码，旋转次序是Z Y X   q=(cos(0.5roll)-isin(0.5roll)(cos(0.5pitch)-jsin(0.5pitch))(cos(0.5yaw)-jsin(0.5yaw)))
自己推倒可将上式展开*/
void init_dcm(float* init_angle,float* d)
{
  float init_yaw,init_pitch,init_roll=0;
  float cosroll,sinroll,cospitch,sinpitch,cosyaw,sinyaw;
  init_yaw=init_angle[0];
  init_pitch=init_angle[1];
  init_roll=init_angle[2];
  if(init_yaw>pi)
  
  {
    init_yaw=init_yaw-2*pi;
  }
  cosroll=cos(0.5*init_roll);
  sinroll=sin(0.5*init_roll);
  
  cospitch=cos(0.5*init_pitch);
  sinpitch=sin(0.5*init_pitch);
  
  cosyaw=cos(0.5*init_yaw);
  sinyaw=sin(0.5*init_yaw);
  /*Z Y X旋转*/
  d[0]=cosroll*cospitch*cosyaw+sinroll*sinpitch*sinyaw;
  d[1]=sinroll*cospitch*cosyaw-cosroll*sinpitch*sinyaw;
  d[2]=cosroll*sinpitch*cosyaw+sinroll*cospitch*sinyaw;
  d[3]=cosroll*cospitch*sinyaw-sinroll*sinpitch*cosyaw;
}
/*姿态跟新算法，采用比卡的3阶近似*/
void DCM(float* compass,int flag,float* dd,float* W,float* cbn,float* ff,float T,float* angle,float* q_error)
{
  float q0,q1,q2,q3;
  float temp;
  
  float dtemp[16]={0};
  
  float pitch=0;
  float g_temp=0;
  
  DX=(W[0])*T*0.01745;
  DY=(W[1])*T*0.01745;
  DZ=(W[2])*T*0.01745;
  
  temp=(DX*DX+DY*DY+DZ*DZ);
  g_temp=sqrt(compass[0]*compass[0]+compass[1]*compass[1]+compass[2]*compass[2]);
  /*判断是不是AHRS的初始化数据，如果是直接给四元数赋成init_dcm中得到的四元数，如果不是进行姿态解算*/
  if(flag==1)
  {
    d[0]=dd[0];
    d[1]=dd[1];
    d[2]=dd[2];
    d[3]=dd[3];
  }
  else
  {
      /*采用比卡算法的3阶展开，Q（k+1）=[I(1-temp^2/8)+(0.5-temp^2/48)sita.*]Q(k),可见秦永元《惯性导航》302页
      有人用四阶龙格库塔展开，这里不用是因为，龙格库塔需要四此采样的数据*/
    dtemp[0]=1*(1-0.125*temp);
    dtemp[1]=-(0.5-temp*0.0208)*DX;
    dtemp[2]=-(0.5-temp*0.0208)*DY;
    dtemp[3]=-(0.5-temp*0.0208)*DZ;
    
    dtemp[4]=(0.5-temp*0.0208)*DX;
    dtemp[5]=1*(1-0.125*temp);
    dtemp[6]=(0.5-temp*0.0208)*DZ;
    dtemp[7]=-(0.5-temp*0.0208)*DY;
    
    dtemp[8]=(0.5-temp*0.0208)*DY;
    dtemp[9]=-(0.5-temp*0.0208)*DZ;
    dtemp[10]=1*(1-0.125*temp);
    dtemp[11]=(0.5-temp*0.0208)*DX;
    
    dtemp[12]=(0.5-temp*0.0208)*DZ;
    dtemp[13]=(0.5-temp*0.0208)*DY;
    dtemp[14]=-(0.5-temp*0.0208)*DX;
    dtemp[15]=1*(1-0.125*temp);
    
    q0=dtemp[0]*d[0]+dtemp[1]*d[1]+dtemp[2]*d[2]+dtemp[3]*d[3];
    q1=dtemp[4]*d[0]+dtemp[5]*d[1]+dtemp[6]*d[2]+dtemp[7]*d[3];
    q2=dtemp[8]*d[0]+dtemp[9]*d[1]+dtemp[10]*d[2]+dtemp[11]*d[3];
    q3=dtemp[12]*d[0]+dtemp[13]*d[1]+dtemp[14]*d[2]+dtemp[15]*d[3];
    
    /*四元数归一*/
    model_q=sqrt(q0*q0+q1*q1+q2*q2+q3*q3);//四元数模
    d[0]=q0/model_q;
    d[1]=q1/model_q;
    d[2]=q2/model_q;
    d[3]=q3/model_q;
  }
  
  q0=d[0];
  q1=d[1];
  q2=d[2];
  q3=d[3];
  /*下面是对卡尔曼滤波的限制，包括倾角超过+-45°（倾角过大时，角度会因为求解公式的问题出现误差增大），振动过大情况下。*/
  if((asin(-2*(q1*q3-q0*q2))>0.78539816339745)||(asin(-2*(q1*q3-q0*q2))<-0.78539816339745))
  {
    yuce_only=1;
  }

  
  acc_filterUpdate_UD(dtemp,d,ff,q_error);
  yuce_only=0;

  
  /*根据卡尔曼的反馈求的四元数估计值*/
  q0=d[0]+q_error[0];
  q1=d[1]+q_error[1];
  q2=d[2]+q_error[2];
  q3=d[3]+q_error[3];
  
  model_q=sqrt(q0*q0+q1*q1+q2*q2+q3*q3);
  d[0]=q0/model_q;
  d[1]=q1/model_q;
  d[2]=q2/model_q;
  d[3]=q3/model_q;
  
  q0=d[0];
  q1=d[1];
  q2=d[2];
  q3=d[3];
  /*求解姿态变化矩阵CNB,载体系到导航系*/
  TT[0][0]=q0*q0+q1*q1-q2*q2-q3*q3;
  TT[0][1]=2*(q1*q2-q0*q3);
  
  TT[0][2]=2*(q1*q3+q0*q2);
  TT[1][0]=2*(q1*q2+q0*q3);
  TT[1][1]=q0*q0-q1*q1+q2*q2-q3*q3;
  
  TT[1][2]=2*(q2*q3-q0*q1);
  TT[2][0]=2*(q1*q3-q0*q2);
  TT[2][1]=2*(q2*q3+q0*q1);
  
  TT[2][2]=q0*q0-q1*q1-q2*q2+q3*q3;
  
  
  cbn[0]=TT[0][0];
  cbn[1]=TT[0][1];
  cbn[2]=TT[0][2];
  cbn[3]=TT[1][0];
  cbn[4]=TT[1][1];
  cbn[5]=TT[1][2];
  cbn[6]=TT[2][0];
  cbn[7]=TT[2][1];
  cbn[8]=TT[2][2];
  
  /*求解姿态角度*/
  pitch=asin(-TT[2][0]);  
  {
    angle[1]=pitch;
    angle[2]=atan2((cbn[7]),(cbn[8]));
    angle[0]=atan2(cbn[3],cbn[0]);
  }
}
//请注意上面的角度都是rad格式，请自己转换成°
/*系统开机时候得陀螺零位自标定，运行时应保持静止，如达不到此条件，请将main程序中将此段程序剔除*/
void mpu_calibration()
{

  int gryx[16]={0};
  int gryy[16]={0};
  int gryz[16]={0};
  float mpusumx=0;
  float mpusumy=0;
  float mpusumz=0;
  
  float mpuxx[100]={0};
  float mpuyy[100]={0};
  float mpuzz[100]={0};
  float mpusumxx=0;
  float mpusumyy=0;
  float mpusumzz=0;
  int i,j=0;
  for(i=0;i<100;i++)
  {      
     mpusumx=0;
     mpusumy=0;
     mpusumz=0;
     for(j=0;j<16;j++)
     {
       MPU6050ReadGyro(gyrodatasss);
       gryoo[0]= gyrodatasss[0];
       gryoo[1]= gyrodatasss[1];
       gryoo[2]= gyrodatasss[2];
       gryx[j]=gryoo[0];
       gryy[j]=gryoo[1];
       gryz[j]=gryoo[2];
       mpusumx+=gryx[j];
       mpusumy+=gryy[j];
       mpusumz+=gryz[j];
    }
    mpuxx[i]=mpusumx*0.0625;
    mpuyy[i]=mpusumy*0.0625;
    mpuzz[i]=mpusumz*0.0625;
    mpusumxx+=mpuxx[i];
    mpusumyy+=mpuyy[i];
    mpusumzz+=mpuzz[i];
  }
/*求和取均值*/
  mpusumxx*=0.01;
  mpusumyy*=0.01;
  mpusumzz*=0.01;
  mpuoffsetx=mpusumxx;
  mpuoffsety=mpusumyy;
  mpuoffsetz=mpusumzz;
/*初始化状态协方差矩阵，因为是直接估计法，PP矩阵选取的比较小*/
  PP[0]=0.001;
  PP[5]=0.001;
  PP[10]=0.001;
  PP[15]=0.001;
  
}



